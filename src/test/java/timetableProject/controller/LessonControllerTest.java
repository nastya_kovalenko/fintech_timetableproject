package timetableProject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import timetableProject.model.Lesson;
import timetableProject.repository.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class LessonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @AfterEach
    void tearDown() {
        courseRepository.deleteAll();
        userRepository.deleteAll();
        teacherRepository.deleteAll();
        categoryRepository.deleteAll();
        lessonRepository.deleteAll();
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void getAllLessonsSuccessForAdmin() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(lesson);

        String expectedContent = objectMapper.writeValueAsString(lessonList);

        this.mockMvc.perform(get("/api/lessons")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void getAllLessonsSuccessForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(lesson);

        String expectedContent = objectMapper.writeValueAsString(lessonList);

        this.mockMvc.perform(get("/api/lessons")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void getByIdSuccessForAdmin() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String expectedContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(get("/api/lessons/1")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void getByIdSuccessForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String expectedContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(get("/api/lessons/1")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void createLessonSuccessForAdmin() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String requestContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(post("/api/lessons/createLesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void createLessonSuccessForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String requestContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(post("/api/lessons/createLesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('igor', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('igor', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'igor', 20, 'offline' )")
    void createLessonForbiddenForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String requestContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(post("/api/lessons/createLesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void updateLessonSuccessForAdmin() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("other descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String requestContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(put("/api/lessons/editLesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void updateLessonSuccessForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("other descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String requestContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(put("/api/lessons/editLesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('igor', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('igor', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'igor', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'igor', '', 'course')")
    void updateLessonForbiddenForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("other descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        String requestContent = objectMapper.writeValueAsString(lesson);

        this.mockMvc.perform(put("/api/lessons/editLesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void deleteLessonSuccessForUser() throws Exception {
        this.mockMvc.perform(delete("/api/lessons/deleteById/1")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('igor', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('igor', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'igor', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void deleteLessonForbiddenForUser() throws Exception {
        this.mockMvc.perform(delete("/api/lessons/deleteById/1")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void deleteLessonSuccessForAdmin() throws Exception {
        this.mockMvc.perform(delete("/api/lessons/deleteById/1")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void deleteAllSuccessForAdmin() throws Exception {
        this.mockMvc.perform(delete("/api/lessons/deleteAll")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void deleteAllForbiddenForUser() throws Exception {
        this.mockMvc.perform(delete("/api/lessons/deleteAll")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

}