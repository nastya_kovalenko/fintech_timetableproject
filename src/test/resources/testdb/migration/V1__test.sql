CREATE TABLE IF NOT EXISTS users
(
    username TEXT PRIMARY KEY,
    password TEXT
);

CREATE TABLE IF NOT EXISTS teachers
(
    username TEXT PRIMARY KEY,
    fullName TEXT,
    age      INT,
    CONSTRAINT FK_teacher_user FOREIGN KEY (username) REFERENCES users (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS categories
(
    categoryName   TEXT PRIMARY KEY,
    parentCategory TEXT,
    CONSTRAINT FK_parent_child FOREIGN KEY (parentCategory) REFERENCES categories (categoryName)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS courses
(
    name               TEXT PRIMARY KEY,
    description        TEXT,
    category           TEXT,
    supervisorUsername TEXT,
    studentsCount      INT,
    type               TEXT,
    CONSTRAINT FK_course_supervisor FOREIGN KEY (supervisorUsername) REFERENCES teachers (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT FK_course_category FOREIGN KEY (category) REFERENCES categories (categoryName)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS lessons
(
    id              BIGSERIAL,
    name            TEXT,
    description     TEXT,
    teacherUsername TEXT,
    dateTime        TEXT,
    courseName      TEXT,
    CONSTRAINT FK_lessons_teachers FOREIGN KEY (teacherUsername) REFERENCES teachers (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT FK_lessons_courses FOREIGN KEY (courseName) REFERENCES courses (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);