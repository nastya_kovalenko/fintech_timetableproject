package timetableProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import timetableProject.model.Course;
import timetableProject.model.Lesson;
import timetableProject.model.Teacher;
import timetableProject.model.User;
import timetableProject.service.CourseService;
import timetableProject.service.LessonService;
import timetableProject.service.TeacherService;
import timetableProject.service.UserService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;
    private final UserService userService;
    private final TeacherService teacherService;
    private final LessonService lessonService;

    @GetMapping("/courses")
    public ResponseEntity<List<Course>> getAllCourses() {
        try {
            List<Course> courses = courseService.findAll();
            if (courses.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(courses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/courses/{name}")
    public ResponseEntity<Course> getCourseByName(@PathVariable("name") String name) {
        Course course = courseService.findByName(name);
        if (course != null) {
            return new ResponseEntity<>(course, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/courses/createCourse")
    public ResponseEntity<String> createCourse(@RequestBody @Valid Course course) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            if (course.getSupervisorUsername().equals(user.getUsername()) || user.getUsername().equals("admin")) {
                courseService.save(new Course(course.getName(),
                        course.getDescription(), course.getCategory(),
                        course.getSupervisorUsername(), course.getStudentsCount(),
                        course.getLessons(), course.getType()));
                return new ResponseEntity<>("Курс был успешно создан.", HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>("Куратором, создаваемого вами курса должны быть вы", HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается создать курс.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/courses/createCourseByCopying/{courseToCopyName}/{newCourseName}")
    public ResponseEntity<String> createByCopying(@PathVariable("courseToCopyName") String courseName,
                                                  @PathVariable("newCourseName") String newName){
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            Course courseForCopy = courseService.findByName(courseName);
            List<Lesson> lessons = new ArrayList<>();
            for (Lesson lesson : courseForCopy.getLessons()) {
                Lesson newLesson = new Lesson();
                newLesson.setName(lesson.getName());
                newLesson.setDescription(lesson.getDescription());
                newLesson.setTeacherUsername(lesson.getTeacherUsername());
                newLesson.setCourseName(newName);
                newLesson.setDateTime("");
                lessonService.save(newLesson);
                lessons.add(newLesson);
            }
            Course newCourse = new Course(newName, courseForCopy.getDescription(), courseForCopy.getCategory(),
                    userDetails.getUsername(), courseForCopy.getStudentsCount(),
                    lessons, courseForCopy.getType());
            courseService.save(newCourse);
            return new ResponseEntity<>("Курс успешно создан.", HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>("Не удается создать курс.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/courses/update")
    public ResponseEntity<String> updateCourse(@RequestBody @Valid Course course) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            if (user.getUsername().equals("admin")) {
                courseService.update(course);
                return new ResponseEntity<>("Курс успешно обновлен", HttpStatus.OK);
            }
            Teacher teacher = teacherService.findByUsername(user.getUsername());
            Optional<Course> currentCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(course.getName())).findAny();
            if (currentCourse.isPresent()) {
                courseService.update(course);
                return new ResponseEntity<>("Курс успешно обновлен", HttpStatus.OK);
            }
            return new ResponseEntity<>("Вы не можете обновлять этот курс", HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается обновить курс", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/courses/addLessonToCourse/{c_name}/{l_id}")
    public ResponseEntity<String> addLessonToCourse(@PathVariable("c_name") String c_name, @PathVariable("l_id") long l_id ) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            Lesson lesson = lessonService.findById(l_id);
            if (user.getUsername().equals("admin")) {
                lesson.setCourseName(c_name);
                lessonService.update(lesson);
                return new ResponseEntity<>("Занятие успешно добавлено к курсу", HttpStatus.OK);
            }
            Teacher teacher = teacherService.findByUsername(user.getUsername());
            Optional<Course> currentCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(c_name)).findAny();
            Optional<Course> lessonCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(lesson.getCourseName())).findAny();
            if (currentCourse.isPresent() && lessonCourse.isPresent()) {
                lesson.setCourseName(c_name);
                lessonService.update(lesson);
                return new ResponseEntity<>("Занятие успешно добавлено к курсу", HttpStatus.OK);
            }
            return new ResponseEntity<>("Вы не можете редактирвать этот курс", HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается добавить занятие к курсу", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/courses/deleteByName/{courseName}")
    public ResponseEntity<String> deleteCourse(@PathVariable("courseName") String courseName) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            if(user.getUsername().equals("admin")){
                courseService.deleteByName(courseName);
                return new ResponseEntity<>("Курс был успешно удален", HttpStatus.OK);
            }
            Teacher teacher = teacherService.findByUsername(user.getUsername());
            Optional<Course> currentCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(courseName)).findAny();
            if(currentCourse.isPresent()) {
                courseService.deleteByName(courseName);
                return new ResponseEntity<>("Курс был успешно удален", HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>("Вы не можете удалить этот курс", HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается удалить курс", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/courses/deleteAll")
    public ResponseEntity<String> deleteAllLessons() {
        try {
            courseService.deleteAll();
            return new ResponseEntity<>("Все курсы успешно удалены", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается удалить курсы", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
