package timetableProject.model;

import lombok.*;

import javax.validation.constraints.NotNull;


/**
 * модель занятия
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Lesson {

    /**
     * id занятия
     */
    @Getter
    @Setter
    @NotNull(message = "Id занятия не может быть пустым")
    private long id;

    /**
     * название
     */
    @Getter
    @Setter
    @NotNull(message = "Название занятия не может быть пустым")
    private String name;

    /**
     * описание
     */
    @Getter
    @Setter
    @NotNull(message = "Описание занятия не может быть пустым")
    private String description;

    /**
     * преподаватель
     */
    @Getter
    @Setter
    @NotNull(message = "У занятия должен быть преподаватель")
    private String teacherUsername;

    /**
     * дата и время проведения
     */
    @Getter
    @Setter
    private String dateTime;

    /**
     * id курса, которому принадлежит занятие
     */
    @Setter
    @Getter
    @NotNull(message = "Занятие должно принадлежать какому-то курсу")
    private String courseName;

}
