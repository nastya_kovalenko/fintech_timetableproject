package timetableProject.model;

import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * категория курса
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    /**
     * название
     */
    @Getter
    @Setter
    @NotNull(message = "У категории должно быть название")
    private String categoryName;
    /**
     * название родительской категории
     */
    @Getter
    @Setter
    private String parentCategory;
}
