package timetableProject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import timetableProject.service.UserService;

@Configuration
@EnableConfigurationProperties
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/registration").not().fullyAuthenticated()
                .antMatchers(HttpMethod.GET,"/**").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/courses/createCourse").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/courses/createCourseByCopying/*").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/courses/*").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/courses/deleteByName/*").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/courses/deleteAll").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/lessons/createLesson").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/lessons/editLesson").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/lessons/deleteById/*").hasAnyAuthority("ROLE_TEACHER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/lessons/deleteAll").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/teachers/*").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/teachers/*").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/teachers").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/teachers/*").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/categories").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/categories").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/categories/*").hasAuthority("ROLE_ADMIN")
                .and().httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder builder)
            throws Exception {
        builder.userDetailsService(userService);
    }
}

