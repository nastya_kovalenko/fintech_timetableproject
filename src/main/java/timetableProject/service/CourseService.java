package timetableProject.service;

import org.springframework.stereotype.Service;
import timetableProject.model.Course;
import timetableProject.repository.CourseRepository;

import java.util.List;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    public CourseService(CourseRepository repository){
        this.courseRepository = repository;
    }

    public void save(Course course){
        courseRepository.save(course);
    }

    public void update(Course course){
        courseRepository.update(course);
    }

    public Course findByName(String name){
        return courseRepository.findByName(name);
    }

    public void deleteByName(String name){
        courseRepository.deleteByName(name);
    }

    public List<Course> findAll(){
        return courseRepository.findAll();
    }

    public void deleteAll(){
        courseRepository.deleteAll();
    }
}
