package timetableProject.service;

import org.springframework.stereotype.Service;
import timetableProject.model.Lesson;
import timetableProject.repository.LessonRepository;

import java.util.List;

@Service
public class LessonService {

    private final LessonRepository lessonRepository;

    public LessonService(LessonRepository repository){
        this.lessonRepository = repository;
    }

    public void save(Lesson lesson){
        lessonRepository.save(lesson);
    }

    public void update(Lesson lesson){
        lessonRepository.update(lesson);
    }

    public Lesson findById(Long id){
        return lessonRepository.findById(id);
    }

    public void deleteById(Long id){
        lessonRepository.deleteById(id);
    }

    public List<Lesson> findAll(){
        return lessonRepository.findAll();
    }

    public void deleteAll(){
        lessonRepository.deleteAll();
    }

    public List<Lesson> findByTeacherUsername(String username){
        return lessonRepository.findByTeacherUsername(username);
    }
}
