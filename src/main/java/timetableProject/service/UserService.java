package timetableProject.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import timetableProject.repository.UserRepository;
import timetableProject.model.User;

import java.util.List;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username);

        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        List<SimpleGrantedAuthority> authorities;

        if(user.getUsername().equals("admin")){
            authorities = List.of(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }
        else {
            authorities = List.of(new SimpleGrantedAuthority("ROLE_TEACHER"));
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }

    public User findByUsername(String username){
       return repository.findByUsername(username);
    }

    public void save(User user){

        repository.save(user);
    }

}