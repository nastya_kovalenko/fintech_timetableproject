package timetableProject.repository;

import org.apache.ibatis.annotations.Mapper;
import timetableProject.model.Category;

import java.util.List;

@Mapper
public interface CategoryRepository {
    List<Category> findAll();
    void save(Category category);
    void update(Category category);
    void deleteCategory(String name);
    void deleteAll();
}
