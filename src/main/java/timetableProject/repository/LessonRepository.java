package timetableProject.repository;

import org.apache.ibatis.annotations.Mapper;
import timetableProject.model.Lesson;

import java.util.List;

@Mapper
public interface LessonRepository {
    void save(Lesson lesson);
    void update(Lesson lesson);
    Lesson findById(Long id);
    void deleteById(Long id);
    List<Lesson> findAll();
    void deleteAll();
    List<Lesson> findByTeacherUsername(String username);
}
